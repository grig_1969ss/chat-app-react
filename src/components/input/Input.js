import React from "react";
import "./input.css";


export const Input = ({message, setMessage, sendMessage}) => {
    return(
        <form className={'form'}>
            <input
                className={"input"}
                placeholder={"Type message . . . "}
                value={message}
                onChange={e => setMessage(e.target.value)}
                onKeyPress={e => e.key === "Enter" ? sendMessage(e): null}
                type="text"
            />
            <button className={"sendButton"} onClick={e => sendMessage(e)}>Send</button>
        </form>
    )
};